var express = require('express')
  , path = require('path')
  , http = require('http')
  , fs = require('fs');
var app = express();

app.use(express.static('dist'));

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.get('*', function (req, res) {
        console.log('asdfsadf');
    fs.readFile('./dist/index.html', function (err, html) {
        console.log('asdfsadf');
        if (err) {
            throw err;
        }
        res.writeHeader(200, { "Content-Type": "text/html" });
        res.write(html);
        res.end();

    });
});

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});