let webpack = require('webpack');
let path = require('path');

export default {
  debug: true,
  devtool: 'source-map',
  noInfo: false,
  entry: [
    'eventsource-polyfill', // necessary for hot reloading with IE
    'webpack-hot-middleware/client?reload=true', //note that it reloads the page if hot module reloading fails.
    path.resolve(__dirname, 'src/index')
  ],
  target: 'web',
  output: {
    path: __dirname + '/dist', // Note: Physical files are only output by the production build task `npm run build`.
    publicPath: '/',
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: path.resolve(__dirname, 'src')
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],
  module: {
    loaders: [
      {test: /\.js$/, include: path.join(__dirname, 'src'), exclude: path.join(__dirname, 'tools'), loaders: ['babel']},
      //{test: /\.(scss|css)$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader!sass-loader")}, // use for prod
      {test: /\.(scss|css)$/, loaders: ["style-loader", "css-loader", "sass-loader"]},
      {test: /\.(ttf|eot|svg|woff(2)?)(\S+)?$/, loaders: ["file-loader?name=[name].[ext]"]},
      {test: /\.(png|jpg|gif|swf)$/, loaders: ["file-loader?name=[name].[ext]"]}
    ]
  }
};
