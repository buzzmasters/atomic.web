import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/App';
import HomePage from './components/home/HomePage';
import WakeboardingPage from './components/wakeboarding/WakeboardingPage';
import SnowboardingPage from './components/snowboarding/SnowboardingPage';
import SurfingPage from './components/surfing/SurfingPage';
import LongboardingPage from './components/longboarding/LongboardingPage';
import WatchPage from './components/watch/WatchPage';
import VideoUploadPage from './components/uploading/VideoUploadPage';
import Error404 from './components/errors/Page404';
import Error401 from './components/errors/Page401';

export default (
    <Route path="/" component={App}>
         <IndexRoute component={HomePage}/>
         <Route path="/wakeboarding" component={WakeboardingPage}/>
         <Route path="/snowboarding" component={SnowboardingPage}/>
         <Route path="/surfing" component={SurfingPage}/>
         <Route path="/longboarding" component={LongboardingPage}/>
         <Route path="/watch/:id" component={WatchPage}/>
         <Route path="/error/404" component={Error404}/>
         <Route path="/error/401" component={Error401}/>
         <Route path="/upload" component={VideoUploadPage}/>
         <Route path="*" component={Error404} />
    </Route>
);
