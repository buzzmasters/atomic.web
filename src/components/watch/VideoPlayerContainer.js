import React, {Component, PropTypes} from 'react';

class VideoPlayerContainer extends Component {


    render() {
        return (
                <div className="row placeholder-player">
                    <video className="column-12" src={this.props.videoURL} controls="controls" autoPlay></video>
                </div>
        );
    }
}

VideoPlayerContainer.propTypes = {
    videoURL: PropTypes.string
};


export default VideoPlayerContainer;
