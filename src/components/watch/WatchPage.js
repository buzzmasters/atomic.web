import React, {Component, PropTypes} from 'react';
import VideoPlayerContainer from './VideoPlayerContainer';
import WatchHeader from './WatchHeader';
import {sendRequest} from './../../utils/Utils';
import './watchPageStyles.scss';

class WatchPage extends Component {
    constructor() {
    super();
        this.watchLoadVideoDescription = this.watchLoadVideoDescription.bind(this);
        this.state = {
            video: ''
        };

    }

    componentWillMount() {
        this.id = this.props.params.id;
        sendRequest(`http://atomictvvideoservice.azurewebsites.net/api/video/${this.id}`, {
            success: this.watchLoadVideoDescription
        });
    }

    watchLoadVideoDescription(videoResponse) {
        console.log(videoResponse);

        const video = (
            <div>
                <div  className="WatchPage-content">
                    <VideoPlayerContainer
                        videoURL={videoResponse.url}/>
                </div>
                <WatchHeader
                        title={videoResponse.title}
                        videoId={videoResponse.id}
                        author={videoResponse.author}
                        description={videoResponse.description}/>
            </div>
        );


        this.setState({video: video});
    }

    render() {
        return (
            <div>
                {this.state.video}
            </div>
        );
    }
}

WatchPage.propTypes = {
    params: PropTypes.shape({
        id: PropTypes.string.isRequired
    })
};
WatchPage.defaultProps = {};

export default WatchPage;
