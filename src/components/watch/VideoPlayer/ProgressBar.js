import React, { PropTypes, Component } from 'react';

import './ProgressBar.scss';

class ProgressBar extends React.Component {

    static clamp(value, min, max) {
        return Math.min(Math.max(value, min), max);
    }

    constructor (props, context) {
        super(props, context);

        this.handleNoop = this.handleNoop.bind(this);
        this.handleDrag = this.handleDrag.bind(this);
        this.handleStart = this.handleStart.bind(this);
        this.handleEnd = this.handleEnd.bind(this);
        this.setCurrentTime = this.setCurrentTime.bind(this);
    }

    componentDidMount() {
        this.setHandlePosition(this.props.defaultValue);

    }

    handleNoop(e) {
        e.stopPropagation();
        e.preventDefault();
    }

    handleDrag(e) {
        this.handleNoop(e);
        const { onChange } = this.props;

        const value = this.getPercentsValue(e);
        this.setHandlePosition(Math.round(value));
        onChange && onChange(value); // value in percents 0 <-> 100 rounded
    }

    getPercentsValue(e) {
        const progressBarW = this.progressBarSlider.offsetWidth;
        const progressBarX = this.progressBarSlider.offsetLeft;
        const mouseX = e.clientX;

        const value = ProgressBar.clamp(mouseX - progressBarX, 0, progressBarW);

        return value * 100 / progressBarW;
    }

    setHandlePosition(value) {
        const progressBarW = this.progressBarSlider.offsetWidth;
        const pointerW = this.progressBarPointer.offsetWidth;
        const pointerPositionX = value / 100 * progressBarW;

        this.progressBarFill.style.width = pointerPositionX + "px";
        this.progressBarPointer.style.left = (pointerPositionX - pointerW / 2) + "px";
    }


    setCurrentTime(e){
        const progressBarW = this.progressBarSlider.offsetWidth;
        const progressBarX = this.progressBarSlider.offsetLeft;
        const mouseX = e.clientX;

        const value = ProgressBar.clamp(mouseX - progressBarX, 0, progressBarW)*100/progressBarW;

        const durationVideo = this.props.getDurationVideo();
        const currentTime = value * durationVideo / 100;

        this.props.setCurrentTime(currentTime);
    }

    handleEnd() {
        document.removeEventListener('mousemove', this.handleDrag);
        document.removeEventListener('mouseup', this.handleEnd);
    }

    handleStart() {
        document.addEventListener('mousemove', this.handleDrag);
        document.addEventListener('mouseup', this.handleEnd);
    }

    render () {

        return (
            <div className="rangeProgressBar rangeprogressBar-horizontal" ref={(slider) => this.progressBarSlider = slider} onClick={(e) => {this.setCurrentTime(e); this.handleDrag(e); }}>
                <div className="rangeprogressBar__fill" ref={(fill) => this.progressBarFill = fill}></div>
                <div className="rangeprogressBar__pointer"
                     ref={(handle) => this.progressBarPointer = handle}
                     onMouseDown={this.handleStart}></div>
            </div>
        );
    }
}

ProgressBar.propTypes = {
    onChange: React.PropTypes.func,
    setCurrentTime: PropTypes.func,
    getDurationVideo: PropTypes.func,
    defaultValue: React.PropTypes.number
};

ProgressBar.defaultProps = {
    defaultValue: 0
};

export default ProgressBar;
