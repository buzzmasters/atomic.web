import React, {Component, PropTypes} from 'react';

class WatchHeader extends Component {

    render() {
        return (
            <div className="row watch-main-container">
                <div className="watch-header">
                    <div className="watch-headline-title">
                        <h1>{this.props.title}</h1>
                    </div>
                    <div className="watch-user-headline">
                        <span>{this.props.author}</span><br/>
                        <span>{this.props.description}</span>
                    </div>
                </div>
            </div>
        );
    }
}

WatchHeader.propTypes = {
    title: PropTypes.string,
    videoId: PropTypes.string,
    author : PropTypes.string,
    description: PropTypes.string
};

export default WatchHeader;

