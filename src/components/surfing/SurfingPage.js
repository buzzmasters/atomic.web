import React, {Component, PropTypes} from 'react';
import VideosContainer from '../common/VideosContainer';

class SurfingPage extends Component {
    render() {
        return (
            <div className="row">
                <div className="videos-container popular-videos-container">
                    <div className="videos-container-header">
                        <span className="header-title">Surfing most popular</span>
                        <span className="header-description">(based on likes)</span>
                    </div>
                    <VideosContainer sortBy="likes" ascending="false" tag="Surfing"/>
                </div>
            </div>
        );
    }
}

SurfingPage.propTypes = {};
SurfingPage.defaultProps = {};

export default SurfingPage;
