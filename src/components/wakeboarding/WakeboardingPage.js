import React, {Component, PropTypes} from 'react';
import VideosContainer from '../common/VideosContainer';

class WakeboardingPage extends Component {

    render() {
        return (
            <div className="row">
                <div className="videos-container popular-videos-container">
                    <div className="videos-container-header">
                        <span className="header-title">Wakeboarding most popular</span>
                        <span className="header-description">(based on likes)</span>
                    </div>
                    <VideosContainer sortBy="likes" ascending="false" tag="Wakeboarding"/>
                </div>
            </div>
        );
    }
}

WakeboardingPage.propTypes = {};
WakeboardingPage.defaultProps = {};

export default WakeboardingPage;
