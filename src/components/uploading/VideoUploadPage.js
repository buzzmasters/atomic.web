import React, {Component, PropTypes} from 'react';
import {sendRequest} from '../../utils/Utils';
import Auth from '../../utils/Auth';

import './uploadStyles.scss';

class VideoUploadPage extends Component {
    constructor(){
        super();
        this.onFileUpload = this.onFileUpload.bind(this);
        if (!Auth.IsLogged()) {
            location.href = "/error/401";
        }
    }

    onFileUpload() {
        const file = this.fileSelector.files[0];
        const poster = this.imagefileSelector.files[0];
        let formData = new FormData();
        formData.append("File", file);
        formData.append("Poster", poster);
        formData.append("Title", this.title.value);
        formData.append("Description", this.description.value);
        formData.append("Tag", this.tag.value);

        sendRequest("http://atomictvvideoservice.azurewebsites.net/api/video", {
            method: "POST",
            data: formData,
            headers: {
              "Authorization": `bearer ${Auth.GetToken()}`
            },
            success: function(res) {
                location.href = `/watch/${res.id}`;
            },
            error: function(err, res) {
                alert(err);
            }
        });
    }

    render() {
        return (
            <div className="row upload-form">
                <div className="upload-button">
                    <span>Title</span>
                    <input type="text" ref={(t) => this.title = t}/>
                    <span>Description</span>
                    <input type="text" ref={(d) => this.description = d}/>
                    <span>Tag</span>
                    <select ref={(s) => this.tag = s}>
                        <option>Snowboarding</option>
                        <option>Wakeboarding</option>
                        <option>Surfing</option>
                        <option>Longboarding</option>
                    </select>
                        <ul className="upload-file-list">
                            <li>
                                <span>Choose video</span>
                                <input type="file" ref={(file) => this.fileSelector = file} name=""/>
                            </li>
                            <li>
                                <span>Choose poster</span>
                                <input type="file" ref={(file) => this.imagefileSelector = file} name=""/>
                            </li>

                        </ul>
                    <button onClick={this.onFileUpload} className="upload-button-image"></button>
                </div>
            </div>
        );
    }
}

VideoUploadPage.propTypes = {};
VideoUploadPage.defaultProps = {};

export default VideoUploadPage;