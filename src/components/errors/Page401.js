import React, {Component, PropTypes} from 'react';

class Page401 extends Component {

    render() {
        return (
            <div className="row">
                <center><span className="page404" style={{"font-size": "3rem"}}>Page 401: Unauthorized</span></center>
                <div>
                    <iframe width="100%" height="600" src="https://www.youtube.com/v/SIaFtAKnqBU&autoplay=1&loop=1&controls=0&playlist=SIaFtAKnqBU" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        );
    }
}

export default Page401;
