import React, {Component, PropTypes} from 'react';
import VideosContainer from '../common/VideosContainer';

import './homePage.scss';

class HomePage extends Component {

    render() {
        return (
            <div className="row">
                <div className="videos-container popular-videos-container">
                    <div className="videos-container-header">
                        <span className="header-title">Popular</span>
                        <span className="header-description">(based on likes)</span>
                    </div>
                    <VideosContainer sortBy="likes" ascending="false"/>
                </div>
                <div className="videos-container latest-videos-container">
                    <div className="videos-container-header">
                        <span className="header-title">Latest</span>
                        <span className="header-description">(based on publication date)</span>
                    </div>
                    <VideosContainer sortBy="publishDate" ascending="false"/>
                </div>
            </div>
        );
    }
}
HomePage.propTypes = {};
HomePage.defaultProps = {};

export default HomePage;
