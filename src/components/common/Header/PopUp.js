import React, {Component, PropTypes} from 'react';
import SkyLight from 'react-skylight';
import LoginForm from './LoginForm';
import SignUpForm from './SignUpForm';

class PopUp extends Component {

    constructor() {
        super();
        this.state = {
            title: "Login",
            type: "login"
        };

        this.show = this.show.bind(this);
        this.setPopUpType = this.setPopUpType.bind(this);
        this.switchToRegister = this.switchToRegister.bind(this);
        this.switchToLogin = this.switchToLogin.bind(this);
    }

    setPopUpType(state) {
        this.setState({
            title: state.title,
            type: state.type
        });
    }

    switchToRegister() {
        this.setState({
            title: "Register",
            type: "signup"
        });
    }

    switchToLogin() {
        this.setState({
            title: "Login",
            type: "login"
        });
    }

    show() {
        this.dialog.show();
    }

    render() {
        if (this.state.type == "login") {
            const styles = {
                width: '400px',
                height: '320px',
                marginTop: '-160px',
                marginLeft: '-200px'
            };
            return (
                <SkyLight dialogStyles={styles}
                          hideOnOverlayClicked
                          ref={(e) => this.dialog = e}
                          title={this.state.title}>
                    <LoginForm
                        switchToRegister={this.switchToRegister}/>
                </SkyLight>
            );
        } else if (this.state.type == "signup") {
            const styles = {
                width: '400px',
                height: '480px',
                marginTop: '-240px',
                marginLeft: '-200px'
            };
            return (
                <SkyLight dialogStyles={styles}
                          hideOnOverlayClicked
                          ref={(e) => this.dialog = e}
                          title={this.state.title}>
                    <SignUpForm
                        switchToLogin={this.switchToLogin}/>
                </SkyLight>
            );
        }

        return (
            <SkyLight/>
        );
    }
}

PopUp.propTypes = {};
PopUp.defaultProps = {};

export default PopUp;
