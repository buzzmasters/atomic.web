import React, {Component, PropTypes} from 'react';
import Auth from '../../../utils/Auth';

class LoginForm extends Component {

    constructor() {
        super();

        this.onLogin = this.onLogin.bind(this);
    }

    onLogin(e) {
        e.preventDefault();

        Auth.Login(this.username.value, this.password.value, function(res) {
            location.href = location.href;
        });
    }

    render() {
        return (
            <form>
                <div className="row">
                    <div className="large-12 columns">
                        <label>Username
                            <input name="username"
                                   type="text"
                                   required
                                   ref={(username) => this.username = username} />
                        </label>
                    </div>
                </div>
                <div className="row">
                    <div className="large-12 columns">
                        <label>Password
                            <input name="password"
                                   type="password"
                                   required
                                   ref={(password) => this.password = password}
                                   pattern="/^[\S]{6,}$/"/>
                        </label>
                    </div>
                </div>
                <div className="row">
                    <div className="large-12 columns">
                        <button className="button small" onClick={this.onLogin}>Log In</button>
                    </div>
                </div>
                <div className="row">
                    <div className="large-12 columns">
                        <a onClick={this.props.switchToRegister}>Not registered</a>
                    </div>
                </div>
            </form>
        );
    }
}

LoginForm.propTypes = {
    switchToRegister: PropTypes.func
};
LoginForm.defaultProps = {};

export default LoginForm;
