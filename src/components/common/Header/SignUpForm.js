import React, {Component, PropTypes} from 'react';
import Auth from '../../../utils/Auth';

const UsernameRegex = /^[a-zA-Z0-9]{4,}$/;
const PasswordRegex = /^[\S]{6,}$/;
const EmailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

class SignUpForm extends Component {
    constructor() {
        super();

        this.onRegister = this.onRegister.bind(this);
        this.validateForm = this.validateForm.bind(this);
    }

    validateForm() {
        let errors = [];

        if (!this.username.value || !UsernameRegex.test(this.username.value)) {
            errors.push({
                username: "Username must be at least 4 letters and contain only letters and numbers"
            });
        }
        if (!this.password.value || !PasswordRegex.test(this.password.value)) {
            errors.push({
                password: "Password must be at least 6 letters"
            });
        }
        if (!this.email.value || !EmailRegex.test(this.email.value)) {
            errors.push({
                email: "Email does not match pattern"
            });
        }
        if (!this.confirmationPassword.value || !this.password.value
            || this.password.value != this.confirmationPassword.value) {
            errors.push({
                confirmationPassword: "Passwords does not match"
            });
        }

        return errors;
    }

    onRegister(e) {
        e.preventDefault();
        const errors = this.validateForm();

        if (errors.length > 0) {
            throw "Form is not validated";
        }

        Auth.Register(this.username.value, this.email.value, this.password.value, function(res) {
            location.href = location.href;
        });
    }

    render() {
        return (
            <form>
                <div className="row">
                    <div className="large-12 columns">
                        <label>Username
                            <input name="username"
                                   type="text"
                                   required
                                   ref={(username) => this.username = username} />
                        </label>
                    </div>
                </div>
                <div className="row">
                    <div className="large-12 columns">
                        <label>Password
                            <input name="password"
                                   type="password"
                                   required
                                   ref={(password) => this.password = password}
                                   pattern="/^[\S]{6,}$/"/>
                        </label>
                    </div>
                </div>
                <div className="row">
                    <div className="large-12 columns">
                        <label>Confirm Password
                            <input name="confirmationPassword"
                                   type="password"
                                   required
                                   ref={(confirmationPassword) => this.confirmationPassword = confirmationPassword}
                                   pattern="/^[\S]{6,}$/"/>
                        </label>
                    </div>
                </div>
                <div className="row">
                    <div className="large-12 columns">
                        <label>Email
                            <input name="email"
                                   type="email"
                                   required
                                   ref={(email) => this.email = email}/>
                        </label>
                    </div>
                </div>
                <div className="row">
                    <div className="large-12 columns">
                        <button className="button small" onClick={this.onRegister}>Sign Up</button>
                    </div>
                </div>
                <div className="row">
                    <div className="large-12 columns">
                        <a onClick={this.props.switchToLogin}>Already have an account</a>
                    </div>
                </div>
            </form>
        );
    }
}

SignUpForm.propTypes = {
    switchToLogin: PropTypes.func
};
SignUpForm.defaultProps = {};

export default SignUpForm;
