import React, {Component, PropTypes} from 'react';
import Navigation from './Navigation';
import PopUp from './PopUp';
import Auth from '../../../utils/Auth';
import { IndexLink, Link } from 'react-router';

import './header.scss';

class Header extends Component {

    constructor() {
        super();

        this.onLogin = this.onLogin.bind(this);
        this.onLogout = this.onLogout.bind(this);
        this.onRegister = this.onRegister.bind(this);
        this.onAccountClick = this.onAccountClick.bind(this);
    }

    onAccountClick(e) {
        e.preventDefault();
    }

    onLogin() {
        this.popup.setPopUpType({
            title: "Login",
            type: "login"
        });
        this.popup.show();
    }

    onRegister() {
        this.popup.setPopUpType({
            title: "Register",
            type: "signup"
        });
        this.popup.show();
    }

    onLogout() {
        Auth.Logout();
    }

    render() {
        const username = Auth.IsLogged();

        return (
            <header className="header">
                <div className="row">
                    <Navigation/>
                    {username ? (
                        <div className="column auth">
                            <Link to="/upload">
                                <button id="upload-btn" className="button small">UPLOAD &uArr;</button>
                            </Link>
                            <a onClick={this.onAccountClick}>{username}</a>
                            <button id="signup-btn" className="button small" onClick={this.onLogout}>LOGOUT</button>
                        </div>
                    ) : (
                        <div className="column auth">
                            <button id="login-btn" className="button small" onClick={this.onLogin}>LOGIN</button>
                            <button id="signup-btn" className="button small" onClick={this.onRegister}>JOIN</button>
                            <PopUp ref={(e) => this.popup = e}/>
                        </div>
                    )}
                </div>
            </header>
        );
    }
}

Header.propTypes = {};

export default Header;

