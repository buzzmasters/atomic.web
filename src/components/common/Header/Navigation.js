import React, {Component, PropTypes} from 'react';
import { IndexLink, Link } from 'react-router';

class Navigation extends Component {
    render() {
        return (
            <div className="column navigation">
                <nav className="row">
                    <ul className="nav-links">
                        <li className="nav-link">
                            <IndexLink className="header-logo-link" to="/" activeClassName="active">
                                <img src="../static/images/atomic-logo.png"/>
                            </IndexLink>
                        </li>
                        <li className="nav-link">
                            <Link to="/wakeboarding" className="highlightable active-red" activeClassName="active">Wakeboarding</Link>
                        </li>
                        <li className="nav-link">
                            <Link to="/snowboarding" className="highlightable active-blue" activeClassName="active">Snowboarding</Link>
                        </li>
                        <li className="nav-link">
                            <Link to="/surfing" className="highlightable active-lightblue" activeClassName="active">Surfing</Link>
                        </li>
                        <li className="nav-link">
                            <Link to="/longboarding" className="highlightable active-green" activeClassName="active">Longboarding</Link>
                        </li>
                    </ul>
                </nav>
            </div>
        );
    }
}

Navigation.propTypes = {};
Navigation.defaultProps = {};

export default Navigation;
