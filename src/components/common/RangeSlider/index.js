import React, { PropTypes, Component } from 'react';

import './range-slider.scss';

class RangeSlider extends Component {

    static clamp(value, min, max) {
        return Math.min(Math.max(value, min), max);
    }

    constructor (props, context) {
        super(props, context);

        this.handleNoop = this.handleNoop.bind(this);
        this.handleDrag = this.handleDrag.bind(this);
        this.handleStart = this.handleStart.bind(this);
        this.handleEnd = this.handleEnd.bind(this);
    }

    componentDidMount() {
        this.setHandlePosition(this.props.defaultValue);
    }

    handleNoop(e) {
        e.stopPropagation();
        e.preventDefault();
    }

    handleDrag(e) {
        this.handleNoop(e);
        const { onChange } = this.props;

        const value = this.getPercentsValue(e);
        this.setHandlePosition(Math.round(value));
        onChange && onChange(value); // value in percents 0 <-> 100 rounded
    }

    getPercentsValue(e) {
        const sliderH = this.slider.offsetHeight;
        const sliderY = this.slider.offsetTop;
        const mouseY = e.clientY;

        const value = RangeSlider.clamp(sliderH - mouseY + sliderY, 0, sliderH);

        return value * 100 / sliderH;
    }

    setHandlePosition(value) {
        const sliderH = this.slider.offsetHeight;
        const pointerH = this.sliderPointer.offsetHeight;
        const pointerPositionY = value / 100 * sliderH;

        this.sliderFill.style.height = pointerPositionY + "px";
        this.sliderPointer.style.bottom = (pointerPositionY - pointerH / 2) + "px";
    }

    handleEnd() {
        document.removeEventListener('mousemove', this.handleDrag);
        document.removeEventListener('mouseup', this.handleEnd);
    }

    handleStart() {
        document.addEventListener('mousemove', this.handleDrag);
        document.addEventListener('mouseup', this.handleEnd);
    }

    render () {
        return (
            <div className="rangeslider rangeslider-vertical" ref={(slider) => this.slider = slider}>
                <div className="rangeslider__fill" ref={(fill) => this.sliderFill = fill}></div>
                <div className="rangeslider__pointer"
                     ref={(handle) => this.sliderPointer = handle}
                     onMouseDown={this.handleStart}></div>
            </div>
        );
    }
}

RangeSlider.propTypes = {
    onChange: PropTypes.func,
    defaultValue: PropTypes.number
};

RangeSlider.defaultProps = {
    defaultValue: 100
};

export default RangeSlider;