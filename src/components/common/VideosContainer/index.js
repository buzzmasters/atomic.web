import React, {Component, PropTypes} from 'react';
import VideoContainer from './VideoContainer';
import {sendRequest} from '../../../utils/Utils';
import './ContentList.scss';

class VideosContainer extends Component {
    constructor() {
        super();
        this.state = {
            videos: []
        };
        this.loadVideoDescription = this.loadVideoDescription.bind(this);
    }

    componentWillMount(){
        sendRequest('http://atomictvvideoservice.azurewebsites.net/api/video', {
            type: "application/x-www-form-urlencoded",
            data: {
                count: 8,
                sortBy: this.props.sortBy,
                ascending: this.props.ascending,
                tag: this.props.tag
            },
            success: this.loadVideoDescription
        });
    }

    loadVideoDescription(videosResponse)
    {
        const videos = videosResponse.map(function(video) {
            const publishDate = new Date(video.publishDate);
            const publishDateString = `${publishDate.getMonth() + 1}/${publishDate.getDate()}/${publishDate.getFullYear()}`;
            return (
                <li key={video.id}>
                    <VideoContainer title = {video.title}
                                    videoId = {video.id}
                                    author = {video.author}
                                    publishDate = {publishDateString}
                                    posterUrl = {video.posterUrl}/>
                </li>
            );
        });

        this.setState({videos: videos});
    }

    render ()
    {
        return (
            <ul className="allblock">
                {this.state.videos}
            </ul>
        );
    }
}

export default VideosContainer;