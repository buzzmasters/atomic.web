import React, {Component, PropTypes} from 'react';
import VideoDescription from './VideoDescription';
import {Link} from 'react-router';

class VideoContainer extends Component {
    render ()
    {
        return (
            <div className="block">
                <Link to={"/watch/" + this.props.videoId}>
                    <div className="block-hover">
                        <div className="block-hover-play"></div>
                    </div>
                </Link>
                <div className="block-image">
                    <img src={this.props.posterUrl
                        ? this.props.posterUrl
                        : "http://terka.su/wp-content/uploads/2014/12/kartinka-terka_1.jpeg"}/>
                </div>
                <VideoDescription
                    title = {this.props.title}
                    videoId = {this.props.videoId}
                    author = {this.props.author}
                    publishDate = {this.props.publishDate}
                />
            </div>
        );
    }
}

VideoContainer.propTypes = {
    title: PropTypes.string,
    videoId: PropTypes.string,
    author : PropTypes.string,
    publishDate: PropTypes.string,
    posterUrl: PropTypes.string
};

export default VideoContainer;