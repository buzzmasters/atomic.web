import React, {Component, PropTypes} from 'react';

class VideoDescription extends Component {
    render ()
    {
        return (
                <div className="block-description">
                    <span className="video-title"><a href={`/watch/${this.props.videoId}`} title={this.props.title}>{this.props.title}</a></span>
                    <span className="video-author">Author: <a>{this.props.author}</a></span>
                    <span className="video-date">Publish date: {this.props.publishDate}</span>
                </div>
        );
    }
}

VideoDescription.propTypes = {
    title: PropTypes.string,
    videoId: PropTypes.string,
    author : PropTypes.string,
    publishDate: PropTypes.string
};

export default VideoDescription;
