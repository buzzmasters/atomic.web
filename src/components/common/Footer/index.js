import React, {PropTypes} from 'react';

import './footer.scss';

const Footer = () => {
    return (
        <footer>
            <div className="row">
                <div className="small-3 large-3 columns">
                    Some footer text...
                </div>
                <div className="small-3 large-3 columns">
                    Some footer text...
                </div>
                <div className="small-6 large-6 columns">
                    <div>Copyright and use terms</div>
                    <p>All patterns on this site can be used free of charge, but please read this before using them. CC BY-SA 3.0 - Subtle Patterns © Atle Mo.</p>
                </div>
            </div>
        </footer>
    );
};

Footer.propTypes = {};
Footer.defaultProps = {};

export default Footer;
