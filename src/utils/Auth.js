import {setCookie, getCookie, deleteCookie, sendRequest} from './Utils';

//const urlPrefix = "http://172.18.19.59/Atomic.Auth.Service/";
const urlPrefix = "http://atomictvauthservice.azurewebsites.net/";

class Auth {

    static GetToken() {
        const token = getCookie("AtomicTvAccessToken");
        return token;
    }

    static IsLogged() {
        const userName = getCookie("AtomicTvUsername");
        const token = getCookie("AtomicTvAccessToken");

        return userName && token ? userName : false;
    }

    static Register(username, email, password, success, error) {
        sendRequest(urlPrefix + "api/auth/register", {
            method: "POST",
            type: "application/x-www-form-urlencoded",
            data: {
                email,
                username,
                password
            },
            success: function(res) {
                Auth.Login(username, password, function(res) {
                    success && success(res);
                    location.href = location.href;
                });
            },
            error: function(err, res) {
                error && error(res);
            }
        });
    }

    static Login(username, password, success, error) {
        sendRequest(urlPrefix + "token", {
            method: "POST",
            type: "application/x-www-form-urlencoded",
            data: {
                grant_type: "password",
                username: username,
                password: password,
                client_id: "webUi"
            },
            success: function(res) {
                setCookie("AtomicTvAccessToken", res.access_token, {expires: res.expires_in});
                setCookie("AtomicTvUsername", res.userName);
                success && success(res);
            },
            error: function(err, res) {
                error && error(res);
            }
        });
    }

    static Logout() {

        const userName = getCookie("AtomicTvUsername");
        const token = getCookie("AtomicTvAccessToken");

        if (!userName || !token) {
            throw "You are not logged in to log out";
        }

        deleteCookie("AtomicTvAccessToken");
        deleteCookie("AtomicTvUsername");

        location.href = location.href;
    }
}

export default Auth;