let webpack = require('webpack');
let path = require('path');
let ExtractTextPlugin = require("extract-text-webpack-plugin");
let CopyWebpackPlugin = require('copy-webpack-plugin');

const GLOBALS = {
  'process.env.NODE_ENV': JSON.stringify('production')
};

export default {
  debug: true,
  devtool: 'source-map',
  noInfo: false,
  entry: path.resolve(__dirname, 'src/index'),
  target: 'web',
  output: {
    path: __dirname + '/bin/dist',
    publicPath: '/',
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: path.resolve(__dirname, 'bin/dist')
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.DefinePlugin(GLOBALS),
    new ExtractTextPlugin("styles.css"),
    new CopyWebpackPlugin([
      {from: path.resolve(__dirname, 'src/static'), to: 'static'},
      {from: path.resolve(__dirname, 'server'), to: '../'}
    ]),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin()
  ],
  module: {
    loaders: [
      {test: /\.js$/, include: path.join(__dirname, 'src'), exclude: path.join(__dirname, 'tools'), loaders: ['babel']},
      {test: /\.(scss|css)$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader!sass-loader")},
      {test: /\.(ttf|eot|svg|woff(2)?)(\S+)?$/, loaders: ["file-loader?name=[name].[ext]"]},
      {test: /\.(png|jpg|gif|swf)$/, loaders: ["file-loader?name=[name].[ext]"]}
    ]
  }
};
