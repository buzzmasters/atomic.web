import fs from 'fs';
import cheerio from 'cheerio';

fs.readFile('src/index.html', 'utf8', (err, markup) => {
   if (err) {
       return console.log(err);
   }

   const $ = cheerio.load(markup);

    $('head').prepend('<link rel="stylesheet" href="/styles.css"');
    $('head').prepend('<link rel="stylesheet" href="/foundation.min.css"');

    fs.writeFile('bin/dist/index.html', $.html(), 'utf8', function (err) {
        if (err) {
            return console.log(err);
        }

        console.log('index.html written to /dist');
    })
});